(function() {
    
    var canvas = document.getElementById('canvas');
    var context = canvas.getContext('2d');

    var canvasWidth = window.innerWidth;
    var canvasHeight = window.innerHeight;

    var BALL_RADIUS = 4;
    var AREA_PER_BALL = 12500;
    var MAX_ABSOLUTE_DX = 3;
    var MAX_ABSOLUTE_DY = 3;
    var DEFAULT_COLOR = "#444"

    var balls = [];

    function Ball(x, y, dx, dy) {
        this.x = x;
        this.y = y;
        this.dx = dx;
        this.dy = dy;
    }

    function Pair(ball1, ball2) {
        this.ball1 = ball1;
        this.ball2 = ball2;
    }

    function init() {
        window.addEventListener('resize', onResizeWindow, false);
        onResizeWindow();
        drawLoop();
    }

    function onResizeWindow() {
        canvasWidth = window.innerWidth;
        canvasHeight = window.innerHeight;
        canvas.width = canvasWidth;
        canvas.height = canvasHeight;
        repopulateBallsJittered();
    }

    function repopulateBallsRandom() {
        balls = [];
        var numOfBalls = Math.floor(canvasHeight*canvasWidth/AREA_PER_BALL);

        for (var i = 0; i < numOfBalls; i++) {
            balls.push(
                new Ball(
                    getRandom(0+BALL_RADIUS, canvasWidth-BALL_RADIUS),
                    getRandom(0+BALL_RADIUS, canvasHeight-BALL_RADIUS),
                    getRandom(-MAX_ABSOLUTE_DX, MAX_ABSOLUTE_DX),
                    getRandom(-MAX_ABSOLUTE_DY, MAX_ABSOLUTE_DY)
                )
            );
        }
    }

    function repopulateBallsJittered() {
        balls = [];
        var unitDistance = Math.floor(Math.sqrt(AREA_PER_BALL));

        var a = canvasWidth - unitDistance;
        var b = canvasHeight - unitDistance;

        for (var i = 0; i < a; i += unitDistance) {
            for (var j = 0; j < b; j += unitDistance) {
                balls.push(
                    new Ball(
                        getRandom(i+BALL_RADIUS, i+unitDistance-BALL_RADIUS),
                        getRandom(j+BALL_RADIUS, j+unitDistance-BALL_RADIUS),
                        getRandom(-MAX_ABSOLUTE_DX, MAX_ABSOLUTE_DX),
                        getRandom(-MAX_ABSOLUTE_DY, MAX_ABSOLUTE_DY)
                    )
                );
            }
        }
    }

    function repopulateBallsTest() {
        balls = [];
        balls.push(
            new Ball(
                400, 400,
                10, 0
            )
        );
    }

    function getRandom(min, max) {
        return Math.random() * (max - min) + min;
    }

    function drawLoop() {
        context.clearRect(0, 0, canvasWidth, canvasHeight);

        for (var i = 0; i < balls.length; i++) {
            drawBall(balls[i]);
        }

        updateBalls();

        requestAnimationFrame(drawLoop);
    }

    function updateBalls() {
        for (var i = 0; i < balls.length; i++) {
            if(balls[i].x <= 0+BALL_RADIUS)  {
                balls[i].dx = Math.abs(balls[i].dx);
            }

            if(balls[i].x >= canvasWidth-BALL_RADIUS) {
                balls[i].dx = -Math.abs(balls[i].dx);
            }

            if(balls[i].y <= 0+BALL_RADIUS) {
                balls[i].dy = Math.abs(balls[i].dy);
            }

            if(balls[i].y >= canvasHeight-BALL_RADIUS) {
                balls[i].dy = -Math.abs(balls[i].dy);
            }
        }

        balls.sort(function(a, b) {
            return a.x - b.x;
        });

        var collisionPairs = [];

        for (var i = 0; i < balls.length-1; i++) {
            var j = i+1;
            while(j < balls.length && balls[i].x - balls[j].x <= BALL_RADIUS*2) {

                if(  (balls[i].x-balls[j].x)*(balls[i].x-balls[j].x) + (balls[i].y-balls[j].y)*(balls[i].y-balls[j].y) <= BALL_RADIUS*BALL_RADIUS*4) {
                    collisionPairs.push(
                        new Pair(
                            balls[i],
                            balls[j]
                        )
                    );
                }

                j++;
            }
        }

        for (var i = 0; i < collisionPairs.length; i++) {

            var b1 = collisionPairs[i].ball1;
            var b2 = collisionPairs[i].ball2;
            var dirX = b2.x - b1.x;
            var dirY = b2.y - b1.y;

            //dirX and dirY are the X and Y components of the vector that goes between the centers of the two circles

            var dx1 = b1.dx+((b2.dx*dirX+b2.dy*dirY)-(b1.dx*dirX+b1.dy*dirY))/(dirX*dirX+dirY*dirY)*dirX;

            var dy1 = b1.dy+((b2.dx*dirX+b2.dy*dirY)-(b1.dx*dirX+b1.dy*dirY))/(dirX*dirX+dirY*dirY)*dirY;

            var dx2 = b2.dx+((b1.dx*dirX+b1.dy*dirY)-(b2.dx*dirX+b2.dy*dirY))/(dirX*dirX+dirY*dirY)*dirX;

            var dy2 = b2.dy+((b1.dx*dirX+b1.dy*dirY)-(b2.dx*dirX+b2.dy*dirY))/(dirX*dirX+dirY*dirY)*dirY;

            b1.dx = dx1;
            b1.dy = dy1;
            b2.dx = dx2;
            b2.dy = dy2;
        }

        for (var i = 0; i < balls.length; i++) {
            balls[i].x += balls[i].dx;
            balls[i].y += balls[i].dy;
        }
    }

    function drawBall(ball) {
        context.fillStyle = DEFAULT_COLOR;
        context.beginPath();
        context.arc(ball.x, ball.y, BALL_RADIUS, 0, 2*Math.PI, false);
        context.fill();
    }

    init();

})();