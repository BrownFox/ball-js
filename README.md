Simulation of elastic collision of many balls

Uses sweep-and-prune algorithm, along with derived formula for velocity change on collision

Simulation is discrete